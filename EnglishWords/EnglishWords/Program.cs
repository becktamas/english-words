﻿using System;

namespace Bekeres
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
            Console.WriteLine("Hello World!");
            Console.ForegroundColor = ConsoleColor.Red;
            String s;
            Console.Write("Please type your name: ");
            s = Console.ReadLine();
            Console.WriteLine("Hello {0}!", s);
            Console.Write("Kérek egy számot: ");
            s = Console.ReadLine();
            Console.WriteLine("A beolvasott szám: {0}", s);
            int x;
            if (!int.TryParse(s, out x))
            {
                Console.WriteLine("Hiba!");
            }
            else
            {
                Console.WriteLine("A kétszeresének harmada: {0}", 2.0 * x / 3);
            }
            string[] st = { "one", "two", "thre", "four", "five", "six",
                "seven", "eigth", "nine", "ten", "eleven", "twelve",
                "thirteen", "forteen", "fifteen", "sixteen", "seventeen",
                "eightteen", "nineteen", "twenty", "twentyone", "twentytwo",
                "twentythre", "twentyfour", "twentyfive", "twentysix", "twentyseven"};
            for (int i = 0; i < st.Length; i++)
            {
                Console.Write("{0}: ", i + 1);
                for (int j = 0; j < st[i].Length; j++)
                {
                    Console.ReadKey(true);
                    Console.Write(st[i][j]);
                }
                //Console.ReadKey(true);
                Console.WriteLine("\t\tjó: {0}", st[i]);
            }
            ConsoleKey Key = ConsoleKey.Enter;
            do
            {
                Key = Console.ReadKey(true).Key;
            } while (Key != ConsoleKey.Escape);
        }
    }
}